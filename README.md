# 0xFF 模拟位置 
Android采用任意一款模拟位置app即可定位签到
此项脚本只在服务器上定时执行才有意义

# 0x00 基础信息准备

## 整个签到流程全部基于http
```
CLIENT_URL="http://em.gzzg.com.cn:8089/client.do",
LOGIN_ID=用户名,
PASSWORD=密码
UUID=装置ID 可以在 设置  关于中 找到
CLIENT_OS=操作系统具体版本号
CLIENT_OS_VERSION="7" Android 系统哦你版本号
CLIENT_TYPE="android",  
```


## 请求头
```
headers = {
    "Accept-Language": "en-GB,zh;q=0.5",
    "Accept-Charset": "utf-8;q=0.7,*;q=0.7",
    "Accept-Encoding": "gzip",
    "Timezone": "GMT+08:00",
    "Content-Type": "application/x-www-form-urlencoded",
    "User-Agent": "E-Mobile/6.5.46 (Linux;U;Android 2.2.1;zh-CN;Nexus One Build.FRG83) AppleWebKit/553.1(KHTML,like Gecko) Version/4.0 Mobile Safari/533.1",
}
```

# 0x01 流程


##获得JSESSIONID

```
import requests
import loguru

logger = loguru.logger
session = requests.Session()
get_config_data = dict(clientver="6.5.46", clienttype=CLIENT_TYPE, androidShowEScene="1")
get_config_resp = session.post(
    CLIENT_URL,
    headers=headers,
    params=dict(method="getconfig"),
    data=get_config_data,
    timeout=10,
)
```

## 登录

```
headers["Cookie2"] = "$Version=1"
login_data = dict(
    method="login",
    loginid=LOGIN_ID,
    password=PASSWORD,
    isneedmoulds="1",
    client="1",
    clientver="6.5.46",
    udid=UUID,
    token="",
    clientos=CLIENT_OS,
    clientosver=CLIENT_OS_VERSION,
    clienttype=CLIENT_TYPE,
    language="en",
    country="GB",
    authcode="",
    dynapass="",
    tokenpass="",
    relogin="1",
    clientuserid="",
)
login_resp = session.post(CLIENT_URL, headers=headers, data=login_data, timeout=10)
```

## 获取签到情况

```
get_checkin_status_params = dict(method="checkin", type="getStatus")
get_checkin_status_resp = session.get(CLIENT_URL, headers=headers, params=get_checkin_status_params, timeout=10)
get_checkin_status_resp.json()
```
签到情况回应中会含有 签到按钮的使能情况及上次签到结果



## 签到及签退
```
checkin_params = dict(
    method="checkin",
    type="checkin",
    latlng="23.130927,113.544289",
    addr="Guangdong+Guangzhou+Huangpu+Hengda+Road+No.1+Near+Guangzhou+Zhiguang+Electric+Co.%2C+Ltd.",
    sessionkey=session.cookies.get("JSESSIONID"),
    wifiMac="null",
)
checkin_resp = session.get(CLIENT_URL, headers=headers, params=checkin_params, timeout=10)
logger.info("checkin result {}".format(checkin_resp.json()))


checkout_params = dict(
    method="checkin",
    type="checkout",
    latlng="23.129467,113.545916",
    addr="Guangdong+Guangzhou+Huangpu+Punan+Road+No.30+Near+Blue+Moon+Scientific+Research+Office+Building",
    sessionkey=session.cookies.get("JSESSIONID"),
    wifiMac="null",
)
checkout_resp = session.get(CLIENT_URL, headers=headers, params=checkout_params, timeout=10)
logger.info("checkout result {}".format(checkout_resp.json()))
```

# 0x02总结

用户名密码全程通过 http 不加密方式传播
签到时机一般对比上次签到结果来决定即可
